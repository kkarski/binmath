<?php

/**
 * Class BinMath
 *
 * Class containing basic arithmetic operations utilizing
 * bit shifting and binary operators.
 *
 * Note: each function's expected input is unsigned 32-bit integer,
 *       it's caller's responsibility to check for that or you may
 *       experience undefined behaviour
 *
 * @author Krzysztof Karski <krzysztof.karski@gmail.com>
 */
abstract class BinMath
{
    /**
     * Sum two numbers.
     *
     * @param int $a
     * @param int $b
     * @return int
     */
    final public static function add(int $a, int $b) : int
    {
        $carry = $a & $b;
        $result = $a ^ $b;

        while (0 !== $carry) {
            $shiftedCarry = $carry << 1;
            $carry = $result & $shiftedCarry;
            $result ^= $shiftedCarry;
        }

        return $result;
    }

    /**
     * Substract b from a.
     *
     * @param int $a
     * @param int $b
     * @return int
     */
    final public static function sub(int $a, int $b) : int
    {
        return static::add($a, static::add(~ $b, 1));
    }

    /**
     * Multiply a times b.
     *
     * @param int $a
     * @param int $b
     * @return int
     */
    final public static function mult(int $a, int $b) : int
    {
        $i = 32;
        $total = 0;

        while (0 < $i--) {
            $total <<= 1;

            if (($b & (1 << $i)) >> $i) {
                $total = static::add($total, $a);
            }
        }

        return $total;
    }

    /**
     * Divide a by b.
     *
     * @param int $a - dividend
     * @param int $b - divisor
     * @return int
     * @throws DivisionByZeroError
     */
    final public static function div(int $a, int $b) : int
    {
        if (0 === $b) {
            throw new \DivisionByZeroError();
        }

        $i = 32;
        $quotient = 0;
        $remainder = 0;

        while (0 < $i--) {
            $quotient <<= 1;
            $remainder <<= 1;
            $remainder |= ($a & (1 << $i)) >> $i;

            if ($remainder >= $b) {
                $remainder = static::sub($remainder, $b);
                $quotient |= 1;
            }
        }

        return $quotient;
    }
}
